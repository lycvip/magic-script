package org.ssssssss.script.parsing.ast;

import org.ssssssss.script.MagicScriptContext;

public interface VariableSetter {
    public void setValue(MagicScriptContext context, Object value);
}