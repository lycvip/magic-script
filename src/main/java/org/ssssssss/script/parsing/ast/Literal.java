package org.ssssssss.script.parsing.ast;

import org.ssssssss.script.parsing.Span;

/**
 * 常量
 */
public abstract class Literal extends Expression{

	public Literal(Span span) {
		super(span);
	}
}
